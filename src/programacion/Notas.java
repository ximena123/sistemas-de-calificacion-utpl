/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programacion;

import java.io.*;
import java.util.*;

/**
 *
 * @author hxpuchaicela
 */
public class Notas {
    public static void main(String[] args) {
        Scanner teclado = new Scanner (System.in);
        int stu;
        double for1, chat1, vid1, tra1, pre1, for2, chat2, vid2, tra2, pre2, fin1 = 0, fin2 = 0, total, partial1 = 0, partial2 = 0;
        String student, alert = " ", promotion = "Aprobado";
        System.out.println("Ingrese el numero de alumnos: ");stu = teclado .nextInt();
        try {
            Formatter fileOut = new Formatter("File1.csv");
            fileOut.format("%s;%s;%s;%s;%s;%s;%s;%s;%s", "\t", "\t", "\t", "\t", "\t", "\t", "\t", "\t", "NOTAS");
            fileOut.format("\r\n");
            fileOut.format("%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s", "NOMBRES", "FOR1", "CHAT1", "VID1", "TRA1", "PRE1", "FOR2", "CHAT2",
                    "VID2", "TRA2", "PRE2", "FIN1", "FIN2", "TOTAL", "ALERTA", "PROMOCION");
            fileOut.format("\r\n");
            for (int i = 0; i < stu; i++) {
                student = "Estudiante " + (i + 1) + ": ";
                for1 = chat1 = vid1 = for2 = chat2 = vid2 = Random(0, 0);
                tra1 = tra2 = Random(0, 5);
                pre1 = pre2 = Random(0, 13);
                if (pre1 < 8) {
                    fin1 = Random(0, 19);
                    alert = "Rendir final 1";
                    partial1 = fin1;
                } else 
                    partial1 = for1 + chat1 + vid1 + tra1 + pre1;
                if (pre2 < 8) {
                    fin2 = Random(0, 19);;
                    alert = "Rendir final 2";
                    partial2 = fin2;
                } else 
                    partial2 = for2 + chat2 + vid2 + tra2 + pre2;
                if (pre1 < 8 && pre2 < 8) 
                    alert = "Rendir final 1 y 2";
                total = partial1 + partial2;
                if (total < 28) 
                    promotion = "Reprobado";
                fileOut.format("%s;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%s;%s", student, for1, chat1, vid1, tra1,
                        pre1, for2, chat2, vid2, tra2, pre2, fin1, fin2, total, alert, promotion);
                fin1 = fin2 = 0;
                alert = " ";
                promotion = "Aprobado";
                fileOut.format("\r\n");
            }
            fileOut.close();
        } catch (FileNotFoundException e) {
        }
    }
    public static double Random(double from, double to) {
        double number;
        return number = (Math.random() * (to - (from - 1))) + from;
    }
}
